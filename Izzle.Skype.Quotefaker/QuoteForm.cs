﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Izzle.Skype.Quotefaker
{
    public partial class QuoteForm : Form
    {
        private static readonly DateTime Epoch =
            new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public QuoteForm()
        {
            InitializeComponent();

            txtDate.Text = DateTime.Now.ToString("g", new CultureInfo("de-DE"));
            Icon = Properties.Resources.Skype_logo;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(" ");

            DateTime dateTime;

            if (!DateTime.TryParse(txtDate.Text, out dateTime))
            {
                return;
            }

            var skypeMessageFragment = String.Format(
                "<quote author=\"{0}\" timestamp=\"{1}\">{2}</quote>",
                txtName.Text,
                (dateTime.ToUniversalTime() - Epoch).TotalSeconds,
                txtMessage.Text);

            var dataObject = new DataObject();
            dataObject.SetData("System.String", txtMessage.Text);
            dataObject.SetData("Text", txtMessage.Text);
            dataObject.SetData("UnicodeText", txtMessage.Text);
            dataObject.SetData("OEMText", txtMessage.Text);

            dataObject.SetData("SkypeMessageFragment",
                new MemoryStream(Encoding.UTF8.GetBytes(skypeMessageFragment)));

            dataObject.SetData("Locale",
                new MemoryStream(BitConverter.GetBytes(CultureInfo.CurrentCulture.LCID)));

            Clipboard.SetDataObject(dataObject);
        }
    }
}
